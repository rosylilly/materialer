# coding: utf-8
require 'csv'

class GeneratorController < ApplicationController
  def index
  end

  def rev
    render text: `git log -n 1 --relative-date`, content_type: 'text/plain'
  end

  def make
    @package = Package.new
    prefix = params[:p] || 'material'
    colors = params[:c] || []
    width = params[:w].to_i
    height = params[:h].to_i

    @package.prefix = prefix
    colors.values.each do |color|
      material = Material.new
      material.color = color
      material.width = width
      material.height = height
      @package.materials << material
    end

    path = Rails.root + "tmp/#{Time.now.to_i}.zip"
    @package.make_zip(path)
    blob = open(path.to_s, 'rb', &:read)
    FileUtils.rm(path.to_s)

    send_data blob, disposition: 'attachment', filename: path.basename
  end

  def csv
    # めんどくさいのでベタ書き
    base_path = Rails.root + "tmp/#{Time.now.to_i}_csv"
    FileUtils.mkdir base_path

    zip_path = base_path + 'package.zip'
    zip_file = Zip::ZipFile.open(zip_path, Zip::ZipFile::CREATE)

    CSV.parse(params[:csv].read) do |row|
      ext = row[4] || 'jpg'
      filename = base_path + "#{row[0]}.#{ext}"
      material = Material.new
      material.color = row[1].match(/[0-9a-fA-F]+/).to_s
      material.width = row[2].to_i
      material.height = row[3].to_i
      material.send("save_to_#{ext}", filename)
      zip_file.add(filename.basename, filename.to_s)
    end

    zip_file.close

    blob = open(zip_path.to_s, 'rb', &:read)
    FileUtils.remove_entry_secure(base_path.to_s, true)

    send_data blob, disposition: 'attachment', filename: "#{Time.now.to_i}.zip"
  end
end
