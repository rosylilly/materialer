class Package
  def initialize
    @prefix = ''
    @path = ''
    @start_from = 0
    @materials = []
  end
  attr_accessor :prefix
  attr_reader :materials

  def make_zip(path)
    @path = Pathname.new(path)

    zip_file = Zip::ZipFile.open(@path, Zip::ZipFile::CREATE)

    @materials.each_with_index do |material, index|
      file_path = material_file_name(index + @start_from)
      material.save_to_jpg(file_path)
      zip_file.add(file_path.basename, file_path.to_s)
    end

    zip_file.close()
  end

  def material_file_name(index)
    @path.dirname + "#{@prefix}_#{index}.jpg"
  end
end
