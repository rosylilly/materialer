class Material
  FORMAT = Cairo::FORMAT_ARGB32

  def initialize
    @width = 0
    @height = 0
    @color = '000'

    @surface = nil
  end
  attr_accessor :width, :height, :color

  def save_to_png(path)
    @surface = Cairo::ImageSurface.new(FORMAT, @width, @height)

    context = Cairo::Context.new(@surface)
    context.set_source_color("##{@color}")
    context.rectangle(0, 0, @width, @height)
    context.fill

    @surface.write_to_png(path.to_s)
  end

  def save_to_jpg(path)
    path = path.to_s unless path.kind_of?(String)

    save_to_png(path)

    image = Magick::Image.read(path).first
    image.format = 'JPEG'
    image.write(path) do
      self.quality = 100
    end
  end

  def save_to_gif(path)
    path = path.to_s unless path.kind_of?(String)

    save_to_png(path)

    image = Magick::Image.read(path).first
    image.format = 'GIF'
    image.write(path)
  end
end
