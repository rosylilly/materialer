class Color
  def self.from_hex(hex)
    color = ReggieB.convert("##{hex}")
    self.new(*color)
  end

  def initialize(r, g, b)
    @r = r
    @g = g
    @b = b
  end

  def to_a
    [@r, @g, @b]
  end
end
